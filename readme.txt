pinkify 0.0.1
------------
05.12.2020

Desc: Converts every photo to pinky form. 
- Has 187 unique shades of pink.
- Just jpeg

Usage: It's a command line tool. So you should open terminal in the directory that the app locates.
In Windows cmd: pinkify.exe -path="ahmet.jpg"
Other terminals: ./pinkify.exe -path="ahmet.jpg"
Posix env: ./pinkify -path="ahmet.jpg"