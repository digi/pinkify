package main

import (
	"os"
	"log"
	"math"
	"flag"
	"image"
	"image/color"
	"image/jpeg"
)

type (
	jpegMatrix [][]color.Color
)

var (
	path string
	newImg jpegMatrix
)

func (jp *jpegMatrix) initJpg(img image.Image){
	var(
		X = img.Bounds().Size().X
		Y = img.Bounds().Size().Y
	)
	for i := 1; i < X; i++ {
		var temp []color.Color
		for j := 1; j < Y; j++ {
			temp = append(temp, img.At(i, j)) 
		}
		*jp = append(*jp, temp)
	}
}

func readImg(pth string) *os.File{
	f, err := os.Open(pth)
	if err != nil {
		log.Fatal("Can't open file\n", err) 
	}
	return f
}

func createNewFile(content jpegMatrix){
	f, err := os.Create("new.jpg")
	if err != nil {
		log.Fatal("Can't save file.\n", err)
	}
	var (
		X = len(content)
		Y = len(content[0])
		RGBA color.RGBA
		sum int
	)
	img := image.NewRGBA(image.Rectangle{image.Point{0,0}, image.Point{X, Y}})
	for i := 0; i < X; i++ {
		for j := 0; j < Y; j++ {
			RGBA = color.RGBAModel.Convert(newImg[i][j]).(color.RGBA)
			r := int(RGBA.R)
			g := int(RGBA.G)
			b := int(RGBA.B)
			a := RGBA.A
			sum = r + g + b
			percent := int(math.Floor(float64(sum) / 4))
			c := color.RGBA{
				255,
				uint8(percent),
				255,
				a,
			}
			img.Set(i, j, c)
		}
	}
	jpeg.Encode(f, img, nil)
}

func main() {
	flag.StringVar(&path, "path", "", "path for file")
	flag.Parse()
	f := readImg(path)
	defer f.Close()
	img, _, err := image.Decode(f)
	if err != nil {
		log.Fatal("Can't decode. Format should be jpeg\n", err)
	}
	newImg.initJpg(img)
	createNewFile(newImg)
}